# Install Qualys cloud agent w/ basic config
#
# Instantiate as follows (both params required):
#
#   class { 'qualys_cloud_agent':
#     activation_id => 'c2fa0d33-1622-4c9c-a521-468ae3722b72',
#     customer_id   => '71ea69b6-3b6e-4585-8242-2c77bc102b62',
#   }
#
# Optional params:
#
#    proxy_host    => 'host.stanford.edu',
#    proxy_port    => '12345',
#    ensure        => 'absent',
#
# EL hosts will get the RPM from the stanford-tcg repo on yum.stanford.edu
#
# Debian and Ubuntu hosts currently get it delivered locally.
#
class qualys_cloud_agent (
  $activation_id = undef,
  $customer_id   = undef,
  $proxy_host    = undef,
  $proxy_port    = undef,
  $ensure        = 'present',
) {
  if $ensure == 'present' {
    $directory_ensure = 'directory'
    $service_ensure = 'running'
    $service_enable = true
  } else {
    $package_ensure = $::osfamily ? {
      'Debian' => 'purged',
      default  => 'absent',
    }
    $directory_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
  }

  # Both activation_id and customer_id are required
  if ((($activation_id == undef) or ($customer_id == undef)) and ($ensure == 'present')) {
    fail('Qualys cloud agent: both the activation ID and customer ID are required parameters')
  }

  # Only x86_64 installers are available as of June 2017
  if (($::architecture != 'x86_64') and ($::architecture != 'amd64')) {
    fail ('Qualys cloud agent: a 64-bit architecture is required')
  }

  # If providing ANY proxy info, both proxy_host and proxy_port are required
  if (((($proxy_host != undef) and ($proxy_port == undef)) or
      (($proxy_host == undef) and ($proxy_port != undef))) and
      ($ensure == 'present')) {
    fail('Qualys cloud agent: if providing proxy configs, both proxy_host and proxy_port required!')
  }

  # ensure a parent directory for the activation trigger and Debian package (as applicable)
  file { '/etc/opt/qualys_cloud_agent':
    ensure  => $directory_ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    backup  => false,
    recurse => true,
    force   => true,
  }

  # Handle the OS families differently
  case $::osfamily {
    'RedHat': {
      package { 'qualys_cloud_agent':
        ensure => $package_ensure,
        name   => 'qualys-cloud-agent',
      }
      $daemon_cfg = '/etc/sysconfig/qualys-cloud-agent'

      # TEMPORARY to clean up old unneeded stuff
      file { '/etc/opt/qualys_cloud_agent/qualys-cloud-agent.x86_64.rpm':
        ensure => absent,
      }
    }
    'Debian': {
      package { 'qualys_cloud_agent':
        ensure => $package_ensure,
        name   => 'qualys-cloud-agent',
      }
      $daemon_cfg = '/etc/default/qualys-cloud-agent'

      # TEMPORARY to clean up old unneeded stuff
      file { '/etc/opt/qualys_cloud_agent/qualys-cloud-agent.x86_64.deb':
        ensure => absent,
      }
    }
    default: {
      fail ('Qualys cloud agent: unsupported operating system')
    }
  }

  # Main config file
  file { '/etc/qualys/cloud-agent/qualys-cloud-agent.conf':
    ensure  => $ensure,
    content => template('qualys_cloud_agent/etc/qualys/cloud-agent/qualys-cloud-agent.conf.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0660',
    require => Package['qualys_cloud_agent'],
  }

  # hacks for ensuring absent
  if ($ensure == 'present') {
    # Activate agent
    exec { 'activate qualys cloud agent':
      command => "/usr/local/qualys/cloud-agent/bin/qualys-cloud-agent.sh ActivationId=${activation_id} CustomerId=${customer_id} && touch /etc/opt/qualys_cloud_agent/activation_complete",
      creates => '/etc/opt/qualys_cloud_agent/activation_complete',
      require => Package['qualys_cloud_agent'],
      before  => File['/etc/qualys/cloud-agent/qualys-cloud-agent.conf'],
    }
  } else {
    # remove the flag file for activation if absent, to allow later enable
    file { '/etc/opt/qualys_cloud_agent/activation_complete':
      ensure => absent
    }
  }

  # Keep the service running
  service{ 'qualys-cloud-agent':
    ensure     => $service_ensure,
    require    => $ensure ? {
      'present' => Exec['activate qualys cloud agent'],
      default   => Package['qualys_cloud_agent'],
    },
    hasstatus  => true,
    hasrestart => true,
    enable     => $service_enable,
    subscribe  => File[$daemon_cfg],
  }

  # daemon config file - needed only for proxy
  file { $daemon_cfg:
    ensure  => $ensure,
    content => template('qualys_cloud_agent/etc/qualys-cloud-agent.erb'),
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
  }
}

