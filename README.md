# qualys_cloud_agent - Qualys Agent used for detailed Vulnerability Management

This is a puppet module that deploys the Qualys Cloud Agent.

# Prerequisites

* This module assumes that the Qualys Cloud Agent package is available via external repo (http://debian.stanford.edu/debian-stanford)

# Puppet Module Usage
Simply include this module in the puppet code
```bash
include qualys_cloud_agent
```

Or Puppet Module Parameters can be specified in this way:
```bash
class { 'qualys_cloud_agent':
  ensure => 'absent',
}
```

And Puppet Module Parameters can also be specified via Hiera in the following format:
```bash
qualys_cloud_agent::activation_id: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
qualys_cloud_agent::customer_id:   'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
```

## Puppet Module Parameters

### activation_id (String) - detault: undef
Activation ID to be used for the Qualys Agent as provided by the Qualys Administrators (ISO)

### customer_id (String) - detault: undef
Customer ID to be used for the Qualys Agent as provided by the Qualys Administrators (ISO)

### proxy_host (String) - detault: undef
In case the host does not have Public Internet access, specify a proxy host that does.

### proxy_port (String) - detault: undef
In case the host does not have Public Internet access, specify a corresponding proxy port.

### ensure (Enum) - default: 'present'
Determines if qualys_cloud_agent is installed or uninstalled from the system.
```bash
qualys_cloud_agent::ensure:     ['absent', 'present']
```
